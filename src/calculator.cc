#include "calculator.h"
#include <algorithm>
#include <cctype>
#include <cstdlib>
#include <iomanip>
#include <iostream>

Directive Calculator::input(std::string expression) {
	std::transform(std::begin(expression), std::end(expression), 
				   std::begin(expression), [](unsigned char c) {
                        return ::tolower(c);
                    });

	std::string_view view = expression;	/* string_views are generally cheaper
										   and never more expensive to
										   work with than strings */

	std::array<std::string_view, 3> terms; /* To store the terms */

	/* Read first term in input */
	{
		auto [state, term] = next_term_and_advance_view(view);
		
		if(state == ViewState::Empty) {
			if(term == QUIT)
				return Directive::Terminate;

			std::cerr << "Warning: unknown command \'" << expression << "\'\n";
			return Directive::Continue;
		}

		if(!is_valid_register_name(term) || operand_type_of(term) == OperandType::Number) {
			std::cerr << "Warning: invalid register name \'" << term << "\', ignoring\n";
			return Directive::Continue;
		}

		terms[0] = term;
	}

	/* Second term in input */
	{
		auto [state, term] = next_term_and_advance_view(view);

		if(state == ViewState::Empty || !is_valid_operator_name(term)) {
			if(terms[0] == PRINT) {
				if(state != ViewState::Empty)
					std::cerr << "Warning: all but first argument to \'print\' will be ignored\n";
				
				print(term);
			}
			else 
				std::cerr << "Warning: unknown command \'" << expression << "\'\n";

			return Directive::Continue;
		}

		terms[1] = term;
	}

	/* Third term in input */
	{
		auto [state, term] = next_term_and_advance_view(view);

		if(state != ViewState::Empty)
			std::cerr << "Warning: more than 3 words in given input, additional arguments will be ignored\n";
		if(!is_valid_operand_name(term)) {
			std::cerr << "Warning: invalid operand \'" << term << "\', ignoring\n";
			return Directive::Continue;
		}

		terms[2] = term;
	}
	
	auto const& [left_operand, bin_op, right_operand] = terms;
	
	OperandType right_op_type = operand_type_of(right_operand);
	
	/* Get the register on the left hand side of the operator*/
	/* If it is not already present in the map, the [] operator will default construct it */
	auto& left_register = registers_[static_cast<std::string>(left_operand)];

	if(right_op_type == OperandType::Number)
		left_register.push(parse_operator(bin_op), as_double(right_operand));
	else {
		/* If right_operand is a register, it's name must be valid (may not contain '.'
		   or '-' as numbers are allowed to) */
		if(is_valid_register_name(right_operand))
			left_register.push(parse_operator(bin_op), as_register(right_operand));
		else
			std::cerr << "Warning: " << right_operand << " is not a valid register name\n";
	}
						   
	return Directive::Continue;
}

Calculator::ParseResult Calculator::next_term_and_advance_view(std::string_view& view) {
	auto pos = view.find(' ');

	/* if view contains a single word */
	if(pos == std::string_view::npos)
		return { ViewState::Empty, view };

	std::string_view word = view.substr(0, pos); /* First word in view... */
	view.remove_prefix(pos + 1u);				 /* ...and "remove" it from view */
	
	return { ViewState::NonEmpty, word };
}

Register::Operation Calculator::parse_operator(std::string_view view) {
	using namespace std::string_view_literals;
	using Operation = Register::Operation;

	if(view == "add"sv)
		return Operation::Add;
	else if(view == "subtract"sv)
		return Operation::Subtract;
	else
		return Operation::Mult;
}

void Calculator::print(std::string_view reg) const {
	/* Check that the register exists */
	auto it = registers_.find(static_cast<std::string>(reg));
	if(it == std::end(registers_)) {
		std::cerr << "Warning: no register named \'" << reg << "\'\n";
		return;
	}
	
	std::cout << std::setprecision(PRECISION) << reg << ": " 
			  << it->second.evaluate() << "\n";
}

double Calculator::as_double(std::string_view view) {
	return std::atof(view.data());
}

Register const& Calculator::as_register(std::string_view view) {
	return registers_[static_cast<std::string>(view)];
}

/* Operands must be alpha-numeric. '.' included for floating points and '-' for 
   negative numbers */
bool Calculator::is_valid_operand_name(std::string_view name) {
	return is_valid_register_name(name) || is_valid_number(name);
}

/* Operator name given in view is valid if it can be found in the OPERATORS array */
bool Calculator::is_valid_operator_name(std::string_view op) {
	return std::find(std::begin(OPERATORS), std::end(OPERATORS), op) != std::end(OPERATORS);
}

/* Register names may only inlcude alpha-numeric characters */
/* Used in conjunction with operand_type_of to ensure given register name is valid */
bool Calculator::is_valid_register_name(std::string_view name) {
	return std::find_if(std::begin(name), std::end(name), [](char c) {
		return !std::isalnum(c);
	}) == std::end(name);
}

/* Numbers may include digits, periods ('.') and minus signs ('-') */
bool Calculator::is_valid_number(std::string_view number) {
	return std::find_if(std::begin(number), std::end(number), [](char c) {
		return !std::isdigit(c) && c != '.' && c != '-';
	}) == std::end(number);
}

/* If the view is a valid number it will be parsed as such, otherwise it is a
   register (the validity of which is checked separately) */
Calculator::OperandType Calculator::operand_type_of(std::string_view operand) {
	if(is_valid_number(operand))
		return OperandType::Number;

	return OperandType::Register;
}

std::array<std::string_view, 3> const Calculator::OPERATORS {
	std::string_view{"add"},
	std::string_view{"subtract"},
	std::string_view{"multiply"}
};
