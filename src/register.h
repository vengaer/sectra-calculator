#ifndef REGISTER_H
#define REGISTER_H

#pragma once
#include <functional>
#include <variant>
#include <vector>

/* This is used for using std::visit with closures (see README.md) */
template <typename... Ts>
struct visitor : Ts... {
	using Ts::operator()...;
};

template <typename... Ts>
visitor(Ts...) -> visitor<Ts...>;

class Register {
	public:
		/* Supported operations */
		enum class Operation{Add, Subtract, Mult};
		
		/* Push an instruction to the register */
		void push(Operation op, double d);
		void push(Operation op, Register const& other);

		/* Evaluate the value of the register */
		double evaluate() const;
	private:
		using ref_wrapper_t = std::reference_wrapper<Register const>; /* variants and other STL containers cannot hold 
																		 plain  references as we cannot assign to them, 
																		 reference_wrappers solve this. */

		/* An instruction is represented as an binary operation to perform and an 
		   operand to perform it with. The latter can be either a number or
		   another register */
		struct Instruction {
			Operation operation;
			std::variant<double, ref_wrapper_t> operand;
		};
		/* Instructions stored in the register */
		std::vector<Instruction> instructions_{};

		/* Catches cyclical dependencies among registers (see README.md) */
		bool causes_cyclical_dependency(ref_wrapper_t const& other) const;

		/* Compute the value of <left> <op> <right> */
		static double compute(double left, double right, Operation op);

		// Compute value stored in an instruction
		static double resolve_variant(std::variant<double, ref_wrapper_t> const& operand);
};

#endif
