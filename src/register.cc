#include "register.h"
#include <algorithm>
#include <iostream>
#include <memory>

void Register::push(Operation op, double d) {
	instructions_.push_back( { op, d } );
}

void Register::push(Operation op, Register const& other) {
	auto other_wrapper = std::cref(other);

	/* Catch cyclical dependency */
	if(this == std::addressof(other) || causes_cyclical_dependency(other_wrapper)) {
		std::cerr << "Warning: Operation cannot be performed as it would cause "
				  << "a cyclical dependency among registers. \nRequested operation "
				  << "ignored.\n";
	}
	else
		instructions_.push_back( { op, other_wrapper } );
}

double Register::evaluate() const {
	double left_value = 0.0;

	for(auto const& [op, operand] : instructions_) {
		/* Evaluate the right operand */
		double right_value = resolve_variant(operand);

		/* Compute the result */
		left_value = compute(left_value, right_value, op);
	}

	return left_value;
}

double Register::compute(double left, double right, Operation op) {
	switch(op) {
		case Operation::Add:
			left += right;
			break;
		case Operation::Subtract:
			left -= right;
			break;
		case Operation::Mult:
			left *= right;
			break;
	}

	return left;
}

/* Recursive depth first search among registers accessible 
   from *this (see README.md for details) */
bool Register::causes_cyclical_dependency(ref_wrapper_t const& other) const{

	/* Check if this is referenced (possibly indirectly) in an instruction */
	auto referenced_in_instruction = [this](Instruction const& instr) {
										 auto const& operand = instr.operand;

										 if(std::holds_alternative<ref_wrapper_t>(operand)) {
											auto const& reg = std::get<ref_wrapper_t>(operand).get();
											
											/* Compare register reg to this and check all registers referenced by reg*/
											return this == std::addressof(reg) || causes_cyclical_dependency(reg);
										 }
										 /* Instruction holds a number which cannot reference any 
											register (out-degree of an instruction holding a number is always 0) */
										 return false;
									 };

	auto const& other_instr = other.get().instructions_;

	/* Check al instructions stored in other*/
	auto it = std::find_if(std::begin(other_instr),
						   std::end(other_instr),
						   referenced_in_instruction);

	/* If a reference to this is found, we would end up with cyclical dependency */
	return it != std::end(other_instr);
}

double Register::resolve_variant(std::variant<double, ref_wrapper_t> const& operand) {
	/* Evaluate the value stored in the variant. If it is a double, return that, 
	   if it is a register, call evaluate  on it and return the result of that */
	return  std::visit(visitor{
					[](double d) { return d; },
					[](ref_wrapper_t const& ref) { return ref.get().evaluate(); }
				},
				operand
			);
}
