#include "calculator.h"
#include <fstream>
#include <iostream>
#include <string>

int main(int argc, char* argv[]) {
	Calculator calc;

	std::ifstream ifs;	
	
	/* File given */
	if(argc > 1) {
		if(argc > 2)
			std::cerr << "Warning: additional arguments passed to program will be ignored\n";

		std::string file = argv[1];
		ifs.open(file);

		if(!ifs.is_open()) {
			/* File does no exist/has wrong permissions, etc., terminate */
			std::cerr << "Could not open file \'" << file << "\', aborting\n";
			return 1;
		}
	}
	else
		std::cout << "Please enter your input below. Exit by typing quit.\n";
	
	/* If the file stream is open, we want to read from that, otherwise from standard input. */
	/* Using an istream ref to the relevant stream avoids code duplication */
	std::istream& is = ifs.is_open() ? 
							ifs	    :
							std::cin;

	std::string line;
	/* Read line by line, ignoring leading white spaces */
	while(is >> std::ws, std::getline(is, line)) {
		Directive directive = calc.input(line);

		/* User entered "quit" */
		if(directive == Directive::Terminate)
			break;
	}
	
	return 0;
}
