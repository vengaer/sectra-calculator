# Sectra Calculator  

A C++ implementation of the Sectra programming assignment *Simple Calculator*.

[![Build Status](https://gitlab.com/vilhelmengstrom/sectra-calculator/badges/master/pipeline.svg)](https://gitlab.com/vilhelmengstrom/sectra-calculator/commits/master)
[![Build Status](https://ci.appveyor.com/api/projects/status/tpi699miuwrw871a?svg=true)](https://ci.appveyor.com/project/VilhelmEngstrom/sectra-calculator)

**Please make sure this is compiled with the C++17 standard as it relies heavily on features not available in earlier versions.**

The code has been tested with clang 7.0.1, gcc 8.2.1 and msvc 15.8.7. The Makefile provided should work in any Linux environment.

## Content Overview
This file explains the assumptions I made when designing the calculator. It also details the algorithm used to catch cyclical dependencies among registers and how I use templates to generate a visitor for using with std::visit inline with the help of lambdas.


## Assumptions    
- The value of an empty register is 0.
- Instructions are given one at a time. Input such as 
	- A add 5 multiply 3  

  should add 5 to the register A and may disregard the rest of the input.
- Keywords (quit and print) must be entered as the first word in an expression. If the user wishes to terminate the program, quit must be given without any arguments. If more than one argument is passed to print, the additional ones can be ignored.
- Print does not have to work with numbers. As such, the input  
	- print 4  

  may be treated as an ill-formed expression.
- Keywords and operators are themselves valid register names. As such:  
	- quit add 5

  is valid and produces a register called quit holding the value 5. On the same note,

	- quit add  

  will result in a parsing error and no operation will be performed. If the user wants to quit the program, no arguments should be given to quit. If the user wishes to manipulate the 
  register quit, the expression must follow the general form of  

  	- \<register\> \<operation\> \<value/register\>  

  As for print, it is also a valid register name, by the same principle as above. All this leads to statements such as

	- print print  

  (printing the register print) and   

	- print quit  

  (printing the register quit) being perfectly valid.

  Furthermore, print does not create registers, as such

	- print reg

  yields an error if the register named reg does not exist. If it does, it will print the contents of the register. 

  As a result of all of the above, all of the following expressions are valid:  

	- add add subtract	/* add the register subtract to the register add */
	- subtract add -3    /* adds -3 to the register subtract */
	- print add			 /* prints the register add, resulting in -3 */
	- subtract multiply multiply /* multiply the register called subtract by the newly created register multiply (containing 0) */
	- print add			/* prints add, resulting in 0 */


- Instructions are to be carried out in the order they are written, meaning that the resulting expression is left-associative. As a result
	1. A add 5
	2. A multiply 6 

  corresponds to the expression
	- ((A + 5) * 6) 

  which is in line with the examples provided in the assignment.
- Absolute peak performance in terms of run time is not requested. As such, I have chosen to provide more informative error messages and catch errors such as cyclical dependencies (see the next item), even though this adds to the run time (and handles the parsing of each term in the input separately instead of using e.g. a loop).
- Adding registers in a cyclical fashion (as seen below) does not have to be supported as this would, by the nature of lazy evaluation, cause infinite recursion. My implementation detects and prevents this (line 3 in the example below will simply have no effect). Furthermore, a register may not reference itself directly as this would also cause a cycle.

	1. A add B
	2. B add C
	3. C add A

- When calling print, the data should be written to standard out immediately rather than when the program terminates (if the latter was preferred, it could be implemented by writing to a buffer which is then written to standard out when the program exits). 
- Registers may be given as a string of alpha-numeric characters. Upper or lower case or leading digits are valid (and the input is case insensitive as requested). Numbers may be given as integral values or floating points using a period, e.g. 5.5 but no support for C++-style suffices (e.g. 1.f) needs to be provided. The numbers entered may be negative but must not contain any spaces. As such,  
	- A add -3 

  is valid and stores -3 in A whereas

	- A add - 3

  is not (notice the white-space). Similarly, 

	- A add 3.3

  will store the value 3.3 in A whereas

	- A add 3 .3 

  (again, notice the white-space) will store 3 in A. The .3 will be seen as a extra argument and be discarded.
- There is no requirement to adjust the floating point precision with respect to the size of the value to be printed. As such, I have chosen the precision as a constant 20, meaning that floating points such as 5.58 might print with a large number of decimals. This behaviour is more likely to occur the larger the number of decimals the result of the register to be printed has and will most likely not be observable when printing integers or single-decimal floating points.
- As it is not requested, over- and underflow does not need to be handled. Using doubles internally makes the risk of this happening rather small but it can still occur.
- Additional input entered on the same line but after a valid command can be disregarded (quit being a special case that must be given no arguments).
- When reading input from a file, if a line in that file contains the single word "quit", the program should exit. This applies even if the EOF has not been reached.
- If the program is launched with more than one command line argument, it should treat the first argument as the location of the file and may disregard the rest.
- If the file could not be opened due to it not existing, having the wrong permissions, etc., the program should terminate rather than falling back to standard input.

## Outline 
The calculator consists of two major components. The Calculator class itself and the Registers stored therein. Shorter descriptions of these are given below.

### Calculator
The Calculator class serves as the main interface for the user. Internally, it holds a number of registers which are used for storing data or as operands themselves. The latter ones are stored in a binary search tree guaranteeing access in O(log n) time and are generated as needed. If the user enters a previously non-existing register as either the left or right operand in an expression, a new register will be created.

### Register
A register is represented as a vector of instructions. An instruction, in turn, holds the right operand and the operator of an expression, the left operand will at evaluation be represented by the value of the register itself. 

The right operand is internally represented as an std::variant storing either a double or a std::reference\_wrapper to a const-qualified Register instance. When evaluating the current register, it will go through each stored instruction and evaluate its result. When the right operand is another Register (in other words when the variant holds a Register instance), this will be evaluated recursively.

## Extending the Program
Adding another operator is done in 4 steps.
1. Add another enumerator to Register::Operation.
2. Add another case to the switch statement in Register::compute, performing the relevant operation.
3. Add the keyword for the new operator to the static array Calculator::OPERATORS.
4. Add an else-if clause in the function Calculator::parse\_operator that compares the given view against the keyword added to the array in step 3.

## visitor
visitor is a variadic class template used in conjunction with std::visit to access values in an std::variant. I came across this very clever idea just last week in a [blog post by Bartlomiej Filipek](https://www.bfilipek.com/2019/02/2lines3featuresoverload.html#more). 

The class template uses the mixin pattern and pulls the operator()'s of its base classes into scope through a using directive. We can then pass it lambdas at creation and the class generated from the template will inherit from the closures created from the lambdas. That way, we get the functionalities of all those closures in the instance of the generated class.

This method comes with a small caveat, namely that class template type deduction will not work by default. To make this compile, we have to write a deduction guide which is simple enough, we simply tell the compiler to use the types the visitor receives at instantiation as its template arguments.

Once this has been done, we can pass an instance of visitor, generated using lambdas, as the first parameter to std::visit and have all the operator()'s of the closures at our disposal.

## Functions
This sections details the slightly less trivial functions in the program. Reading the code should be enough to understand them as it is not particularly involved, but this might serve as a quicker way to come to terms with the details.

### Register::causes\_cyclical\_dependency(/* Other register */)
This function catches any attempt, deliberate or otherwise, to cause cyclical dependencies among registers. In essence, it relies on the observation that the dependencies of a register can be visualized as an unweighted digraph G = (V,E). Every vertex v &in; V represents an operand (a number or a register) and every edge e &in; E is associated with an operation (from here on disregarded in this discussion as it is irrelevant for detecting cycles). Preventing cyclical dependencies among registers is then as simple as detecting when the introduction of a new edge would result in a cycle in the graph.

Suppose a new instruction is to be pushed to a register, this would correspond to introducing a new directed edge e' = (u,v), where u &in; V. If v &notin; V, it is added to the graph. In order to check whether the introduction of e' would introduce a cycle, a depth-first search starting in v is used. If there is a path from v to u already present in the graph, adding e' would result in a cycle. Trivially, the complexity for this operation is O(|V|) where |V| is the number of vertices in the graph (note that I have chosen to view the dependencies of each register as a separate graph).

Take the following example:  

	1. A add 1
	2. A add B
	3. B add 3
	4. B add C  

This could be expressed as the following digraph (again, operators are omitted and irrelevant in this discussion).

![image](images/graph_ex.jpg)

Suppose the request  

	5. C add A  

is then received. In order to determine whether this would introduce a cycle, we perform a depth first search starting in A. In this case, we would eventually find a path from A to C and thus deny the request.

### Register::resolve\_variant(/* variant type */)
An elegant way of accessing the value stored in the variant in the current instruction. It relies on std::visit which applies the relevant operator() of a functor to a variant type. The most common way of doing this is writing a class containing all these operator()'s. However, you can use the mixin pattern to generate such a class by inheriting from closures instead (see the section detailing visitor for more details on this).

A summary of what the function does is as follows:
- If the variant contains a double, return that double.
- If the variant contains a (reference\_wrapper wrapping a) register, call evaluate on that register and return the resulting value.  
