CXX ?= g++

SRC = $(wildcard src/*.cc)
OBJ := $(addsuffix .o,$(basename $(SRC)))
INC = -I src/
BIN = calc

export CPPFLAGS

CXXFLAGS := $(CXXFLAGS) -std=c++17 -Wall -Wextra -pedantic -Weffc++ $(INC) 

$(BIN): $(OBJ)
	$(CXX) -o $@ $^ $(CXXFLAGS)

.PHONY: clean run clang gcc msvc
clean:
	rm -f $(OBJ) $(BIN)

run: $(BIN)
	./$(BIN)

clang: CXX = clang++
clang: $(BIN)

gcc: CXX = g++
gcc: $(BIN)

msvc: CXX := cl.exe
msvc: OBJ := $(addsuffix obj, $(basename $(OBJ)))
msvc: CXXFLAGS := /std:c++17 /W3 /I src
msvc:
	$(CXX) $(CXXFLAGS) /EHsc $(SRC) /link /out:$(BIN).exe
